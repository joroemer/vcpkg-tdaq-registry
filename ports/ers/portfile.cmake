vcpkg_from_gitlab(
    GITLAB_URL https://gitlab.cern.ch
    OUT_SOURCE_PATH SOURCE_PATH
    REPO atlas-tdaq-software/ers
    REF 8240847cb9c4d0b17fe86ae5d6f0bc377f1f12d1
    SHA512 c4f5ad16e1f372f2ef30e9efe353ec11cf1b366b3d9ff794979694af07bb037069790c96fe5f3d7083ff68f8c9fa49ddd965d9128f7e3c216df0b23d613c5fcf
    HEAD_REF master
)

# Move code to sub directory for TDAQ magic
file(GLOB ALL_CONTENTS RELATIVE ${SOURCE_PATH} ${SOURCE_PATH}/*)
file(MAKE_DIRECTORY ${SOURCE_PATH}/ers_src)
foreach(ITEM ${ALL_CONTENTS})
  message(STATUS "Moving ${ITEM}")
  file(RENAME ${SOURCE_PATH}/${ITEM} ${SOURCE_PATH}/ers_src/${ITEM})
endforeach()

file(WRITE ${SOURCE_PATH}/CMakeLists.txt "
cmake_minimum_required(VERSION 3.27)
project(ers)
if(CMAKE_BUILD_TYPE STREQUAL \"Debug\")
  string(REGEX REPLACE \"-opt|-dbg\" \"-dbg\" BINARY_TAG \"\${BINARY_TAG}\")
else()
  string(REGEX REPLACE \"-opt|-dbg\" \"-opt\" BINARY_TAG \"\${BINARY_TAG}\")
endif()
include(FetchContent)
FetchContent_Declare(
    cmake_tdaq
    GIT_REPOSITORY https://gitlab.cern.ch/atlas-tdaq-felix/cmake_tdaq.git
    GIT_TAG joroemer/conan
)
FetchContent_GetProperties(cmake_tdaq)
if(NOT cmake_tdaq_POPULATED)
    FetchContent_Populate(cmake_tdaq)
endif()
include(\${cmake_tdaq_SOURCE_DIR}/cmake/modules/TDAQ.cmake)
tdaq_project(ers 0.0.0 USES LCG \$ENV{LCG_VERSION})
")

file(WRITE ${SOURCE_PATH}/cmake/ers.cmake.in "
@PACKAGE_INIT@

set(@PROJECT_NAME@_INCLUDE_DIRS \${PACKAGE_PREFIX_DIR}/@VCPKG_TARGET_TRIPLET@/include \${PACKAGE_PREFIX_DIR}/@VCPKG_TARGET_TRIPLET@/external/include)
set(@PROJECT_NAME@_LIBRARY_DIRS \${PACKAGE_PREFIX_DIR}/@VCPKG_TARGET_TRIPLET@/lib \${PACKAGE_PREFIX_DIR}/@VCPKG_TARGET_TRIPLET@/external/lib)
link_directories(\${PACKAGE_PREFIX_DIR}/@VCPKG_TARGET_TRIPLET@/external/lib)
set(@PROJECT_NAME@_TARGETS @TDAQ_ALL_TARGETS@)

include(\${PACKAGE_PREFIX_DIR}/@VCPKG_TARGET_TRIPLET@/share/cmake/@PROJECT_NAME@/@PROJECT_NAME@Targets.cmake)

set_target_properties(@PROJECT_NAME@::@PROJECT_NAME@ PROPERTIES
    INTERFACE_INCLUDE_DIRECTORIES \"\${PACKAGE_PREFIX_DIR}/@VCPKG_TARGET_TRIPLET@/include\"
)
")

vcpkg_configure_cmake(
    SOURCE_PATH ${SOURCE_PATH}
    PREFER_NINJA
    OPTIONS
    -DBINARY_TAG=$ENV{BINARY_TAG}
    )

vcpkg_install_cmake()

vcpkg_copy_pdbs()

vcpkg_install_copyright(FILE_LIST "${SOURCE_PATH}/ers_src/LICENSE")

foreach(BINARY_TAG_SUFFIX dbg opt)
    string(REGEX REPLACE "-opt|-dbg" "-${BINARY_TAG_SUFFIX}" BINARY_TAG "$ENV{BINARY_TAG}")
    if(EXISTS ${CURRENT_PACKAGES_DIR}/${BINARY_TAG})
        file(INSTALL ${CURRENT_PACKAGES_DIR}/${BINARY_TAG}/lib DESTINATION ${CURRENT_PACKAGES_DIR})
        file(INSTALL ${CURRENT_PACKAGES_DIR}/${BINARY_TAG}/bin DESTINATION ${CURRENT_PACKAGES_DIR})
        file(REMOVE_RECURSE "${CURRENT_PACKAGES_DIR}/${BINARY_TAG}")
    endif()

    if(EXISTS ${CURRENT_PACKAGES_DIR}/debug/${BINARY_TAG})
        file(INSTALL ${CURRENT_PACKAGES_DIR}/debug/${BINARY_TAG}/lib DESTINATION ${CURRENT_PACKAGES_DIR}/debug)
        file(INSTALL ${CURRENT_PACKAGES_DIR}/debug/${BINARY_TAG}/bin DESTINATION ${CURRENT_PACKAGES_DIR}/debug)
        file(REMOVE_RECURSE "${CURRENT_PACKAGES_DIR}/debug/${BINARY_TAG}")
    endif()
    
    if(EXISTS ${CURRENT_PACKAGES_DIR}/share/cmake/ers/${BINARY_TAG})
    file(GLOB ERS_FILES "${CURRENT_PACKAGES_DIR}/share/cmake/ers/${BINARY_TAG}/*.cmake")
        file(INSTALL ${ERS_FILES} DESTINATION ${CURRENT_PACKAGES_DIR}/share/cmake/ers)
        file(REMOVE_RECURSE "${CURRENT_PACKAGES_DIR}/share/cmake/ers/${BINARY_TAG}")
    endif()
endforeach()
file(INSTALL ${CURRENT_PACKAGES_DIR}/../ers_src/ers/ DESTINATION ${CURRENT_PACKAGES_DIR}/include/ers)

file(READ ${CURRENT_PACKAGES_DIR}/share/cmake/ers/ersTargets-release.cmake ERS_TARGETS)
foreach(BINARY_TAG_SUFFIX dbg opt)
    string(REGEX REPLACE "-opt|-dbg" "-${BINARY_TAG_SUFFIX}" BINARY_TAG "$ENV{BINARY_TAG}")
    string(REPLACE "${BINARY_TAG}" "x64-linux" ERS_TARGETS ${ERS_TARGETS})
endforeach()
file(WRITE ${CURRENT_PACKAGES_DIR}/share/cmake/ers/ersTargets-release.cmake ${ERS_TARGETS})

file(REMOVE_RECURSE
    "${CURRENT_PACKAGES_DIR}/lib/bin"
    "${CURRENT_PACKAGES_DIR}/lib/cmt"
    "${CURRENT_PACKAGES_DIR}/lib/doc"
    "${CURRENT_PACKAGES_DIR}/lib/dtd"
    "${CURRENT_PACKAGES_DIR}/lib/ers/internal"
    "${CURRENT_PACKAGES_DIR}/lib/pytest"
    "${CURRENT_PACKAGES_DIR}/lib/python"
    "${CURRENT_PACKAGES_DIR}/lib/src/streams"
    "${CURRENT_PACKAGES_DIR}/lib/test"
    "${CURRENT_PACKAGES_DIR}/lib/ers"
    "${CURRENT_PACKAGES_DIR}/lib/src"
    "${CURRENT_PACKAGES_DIR}/debug/share"
    "${CURRENT_PACKAGES_DIR}/ers_src"
    "${CURRENT_PACKAGES_DIR}/share/bin"
)
file(REMOVE "${CURRENT_PACKAGES_DIR}/CMakeLists.txt")
file(REMOVE "${CURRENT_PACKAGES_DIR}/ersConfig.cmake")
file(REMOVE "${CURRENT_PACKAGES_DIR}/share/cmake/sw_repo.yaml")