vcpkg_from_gitlab(
    GITLAB_URL https://gitlab.cern.ch
    OUT_SOURCE_PATH SOURCE_PATH
    REPO atlas-tdaq-software/asyncmsg
    REF f55bf371839f7e95d1a78012f4962b8fa817f65b
    SHA512 235ffa73f2b87c70da2e0e16c657a1586645224c7ed1181d0e1660b7f204155abb3a0af5cf0ab855c938d7571e9ecd3a6861b19f66db13bf91c2d42f6054fb89
    HEAD_REF master
    PATCHES "on-accept.patch" "remove-name-service.patch"
)

# Move code to sub directory for TDAQ magic
file(GLOB ALL_CONTENTS RELATIVE ${SOURCE_PATH} ${SOURCE_PATH}/*)
file(MAKE_DIRECTORY ${SOURCE_PATH}/asyncmsg_src)
foreach(ITEM ${ALL_CONTENTS})
  message(STATUS "Moving ${ITEM}")
  file(RENAME ${SOURCE_PATH}/${ITEM} ${SOURCE_PATH}/asyncmsg_src/${ITEM})
endforeach()

file(WRITE ${SOURCE_PATH}/CMakeLists.txt "
cmake_minimum_required(VERSION 3.27)
project(asyncmsg)
message(STATUS \"Hello\")
if(CMAKE_BUILD_TYPE STREQUAL \"Debug\")
  string(REGEX REPLACE \"-opt|-dbg\" \"-dbg\" BINARY_TAG \"\${BINARY_TAG}\")
else()
  string(REGEX REPLACE \"-opt|-dbg\" \"-opt\" BINARY_TAG \"\${BINARY_TAG}\")
endif()
include(FetchContent)
FetchContent_Declare(
    cmake_tdaq
    GIT_REPOSITORY https://gitlab.cern.ch/atlas-tdaq-felix/cmake_tdaq.git
    GIT_TAG joroemer/conan
)
FetchContent_GetProperties(cmake_tdaq)
if(NOT cmake_tdaq_POPULATED)
    FetchContent_Populate(cmake_tdaq)
endif()

include(\${cmake_tdaq_SOURCE_DIR}/cmake/modules/TDAQ.cmake)
tdaq_project(asyncmsg 0.0.0 USES LCG \$ENV{LCG_VERSION})
")

file(WRITE ${SOURCE_PATH}/cmake/asyncmsg.cmake.in "
@PACKAGE_INIT@

set(@PROJECT_NAME@_INCLUDE_DIRS \${PACKAGE_PREFIX_DIR}/@VCPKG_TARGET_TRIPLET@/include \${PACKAGE_PREFIX_DIR}/@VCPKG_TARGET_TRIPLET@/external/include)
set(@PROJECT_NAME@_LIBRARY_DIRS \${PACKAGE_PREFIX_DIR}/@VCPKG_TARGET_TRIPLET@/lib \${PACKAGE_PREFIX_DIR}/@VCPKG_TARGET_TRIPLET@/external/lib)
link_directories(\${PACKAGE_PREFIX_DIR}/@VCPKG_TARGET_TRIPLET@/external/lib)
set(@PROJECT_NAME@_TARGETS @TDAQ_ALL_TARGETS@)

include(\${PACKAGE_PREFIX_DIR}/@VCPKG_TARGET_TRIPLET@/share/cmake/@PROJECT_NAME@/@PROJECT_NAME@Targets.cmake)

set_target_properties(@PROJECT_NAME@::@PROJECT_NAME@ PROPERTIES
    INTERFACE_INCLUDE_DIRECTORIES \"\${PACKAGE_PREFIX_DIR}/@VCPKG_TARGET_TRIPLET@/include\"
)
")

vcpkg_configure_cmake(
    SOURCE_PATH ${SOURCE_PATH}
    PREFER_NINJA
    OPTIONS
    -DBINARY_TAG=$ENV{BINARY_TAG}
    )

vcpkg_install_cmake()

vcpkg_copy_pdbs()

vcpkg_install_copyright(FILE_LIST "${SOURCE_PATH}/asyncmsg_src/LICENSE")

foreach(BINARY_TAG_SUFFIX dbg opt)
    string(REGEX REPLACE "-opt|-dbg" "-${BINARY_TAG_SUFFIX}" BINARY_TAG "$ENV{BINARY_TAG}")
    if(EXISTS ${CURRENT_PACKAGES_DIR}/${BINARY_TAG})
        file(INSTALL ${CURRENT_PACKAGES_DIR}/${BINARY_TAG}/lib DESTINATION ${CURRENT_PACKAGES_DIR})
        file(REMOVE_RECURSE "${CURRENT_PACKAGES_DIR}/${BINARY_TAG}")
    endif()

    if(EXISTS ${CURRENT_PACKAGES_DIR}/debug/${BINARY_TAG})
        file(INSTALL ${CURRENT_PACKAGES_DIR}/debug/${BINARY_TAG}/lib DESTINATION ${CURRENT_PACKAGES_DIR}/debug)
        file(REMOVE_RECURSE "${CURRENT_PACKAGES_DIR}/debug/${BINARY_TAG}")
    endif()

    if(EXISTS ${CURRENT_PACKAGES_DIR}/share/cmake/asyncmsg/${BINARY_TAG})
    file(GLOB ERS_FILES "${CURRENT_PACKAGES_DIR}/share/cmake/asyncmsg/${BINARY_TAG}/*.cmake")
        file(INSTALL ${ERS_FILES} DESTINATION ${CURRENT_PACKAGES_DIR}/share/cmake/asyncmsg)
        file(REMOVE_RECURSE "${CURRENT_PACKAGES_DIR}/share/cmake/asyncmsg/${BINARY_TAG}")
    endif()
endforeach()
file(INSTALL ${CURRENT_PACKAGES_DIR}/../asyncmsg_src/asyncmsg/ DESTINATION ${CURRENT_PACKAGES_DIR}/include/asyncmsg)

file(READ ${CURRENT_PACKAGES_DIR}/share/cmake/asyncmsg/asyncmsgTargets-release.cmake ASYNCMSG_TARGETS)
foreach(BINARY_TAG_SUFFIX dbg opt)
    string(REGEX REPLACE "-opt|-dbg" "-${BINARY_TAG_SUFFIX}" BINARY_TAG "$ENV{BINARY_TAG}")
    string(REPLACE "${BINARY_TAG}" "x64-linux" ASYNCMSG_TARGETS ${ASYNCMSG_TARGETS})
endforeach()
file(WRITE ${CURRENT_PACKAGES_DIR}/share/cmake/asyncmsg/asyncmsgTargets-release.cmake ${ASYNCMSG_TARGETS})

file(REMOVE_RECURSE
    "${CURRENT_PACKAGES_DIR}/lib/bin"
    "${CURRENT_PACKAGES_DIR}/lib/cmt"
    "${CURRENT_PACKAGES_DIR}/lib/doc"
    "${CURRENT_PACKAGES_DIR}/lib/dtd"
    "${CURRENT_PACKAGES_DIR}/lib/asyncmsg/internal"
    "${CURRENT_PACKAGES_DIR}/lib/pytest"
    "${CURRENT_PACKAGES_DIR}/lib/python"
    "${CURRENT_PACKAGES_DIR}/lib/src/streams"
    "${CURRENT_PACKAGES_DIR}/lib/test"
    "${CURRENT_PACKAGES_DIR}/lib/asyncmsg"
    "${CURRENT_PACKAGES_DIR}/lib/src"
    "${CURRENT_PACKAGES_DIR}/debug/share"
    "${CURRENT_PACKAGES_DIR}/asyncmsg_src"
    "${CURRENT_PACKAGES_DIR}/share/bin"
)
file(REMOVE "${CURRENT_PACKAGES_DIR}/CMakeLists.txt")
file(REMOVE "${CURRENT_PACKAGES_DIR}/asyncmsgConfig.cmake")
file(REMOVE "${CURRENT_PACKAGES_DIR}/share/cmake/sw_repo.yaml")